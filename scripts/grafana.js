// Grafana integration Script
// Matches closely the formatting of the Slack alert integration

const grafana_settings = {
    grafana_url: '[[Your URL]]'
};
class Script {
    /**
     * @params {object} request
     */
    process_incoming_request({request}) {
        try {


            // Format Any Existing EvalMatches to fields for RocketChat Tables
            var fields = [];
            if (request.content.evalMatches.length > 0) {
                for (var i = 0; i < request.content.evalMatches.length; i++) {
                    fields.push({
                            "short": true,
                            "title": String(request.content.evalMatches[i].metric),
                            "value": String(request.content.evalMatches[i].value)
                        }
                    );
                }
            }

            var message = {
                content: {
                    "attachments": [{
                        "color": "#FF0000",
                        "author_name": "Grafana Alerts",
                        "author_link": grafana_settings.grafana_url,
                        "author_icon": "https://grafana.com/img/apple-touch-icon.png",
                        "title": request.content.title,
                        "title_link": request.content.ruleUrl,
                        "text": request.content.message,
                        "fields": fields,
                        "image_url": request.content.imageUrl
                    }]
                }
            };
        }
        catch (e) {
            return {
                error: {
                    success: false,
                    message: e
                }
            };
        }
        return message;
    }
}